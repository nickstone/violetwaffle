import sys, requests, json

def create_assets(host, username, password, verifycert, maxpages, baseurl, assetfilename):
    print "Opening JSON asset file"
    with open(assetfilename) as assetfile:
        assetjson = json.load(assetfile)

    if not verifycert:
        requests.packages.urllib3.disable_warnings() #If disabling SSL cert check, also disable the warnings

    #Post the JSON to the URL
    print "Connecting to Phantom REST API"
    r = requests.post(baseurl, data = json.dumps(assetjson), auth=(username, password), verify = verifycert)
    print "Payload successful"
    print json.dumps(json.loads(r.text), indent=2, sort_keys=True) #Print the results from the server

def main():
    if len(sys.argv) < 2:
        print 'Usage: ' + sys.argv[0] + ' [filename]'
        sys.exit (1)

    assetfilename = sys.argv[1]
    action = sys.argv[2]

    host = sys.argv[3] #IP address or hostname of the Phantom server
    username = sys.argv[4]
    password = sys.argv[5]
    verifycert = False #Default to not checking the validity of the SSL cert, since Phantom defaults to self-signed
    maxpages = 100 #Assume there are no more than 100 pages of results, to protect against an accidental loop
    pagination = '?page='
    baseurl='https://' + host + '/rest/asset' + pagination #The base URL for a list of all assets, by page number
    currentpage = 0

    if action == "import-json":
        create_assets(host, username, password, verifycert, maxpages, baseurl, assetfilename)
    else:
        print "You have chosen an action method that does not exist, please try import-json, import-csv or export"
        sys.exit (1)

if __name__ == "__main__":
    main()
